import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_firebase_auth/pages/about.dart';

import 'package:flutter_firebase_auth/pages/authentication.dart';
import 'package:flutter_firebase_auth/pages/btn.dart';
import 'package:flutter_firebase_auth/pages/func_google.dart';

import 'package:flutter_firebase_auth/pages/login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' show json;

class DrawerSide extends StatefulWidget {
  DrawerSide({Key key}) : super(key: key);

  @override
  State<DrawerSide> createState() => _DrawerSideState();
}

class _DrawerSideState extends State<DrawerSide> {
  FirebaseAuth _auth = FirebaseAuth.instance;

  get user => _auth.currentUser;

  Future<void> _handleSignOut() async => await googleSignIn.signOut();

  @override
  Widget build(BuildContext context) {
    final GoogleSignInAccount userGoogle = googleSignIn.currentUser;

    return Drawer(
      //body drawer
      child: Material(
        color: Color.fromRGBO(50, 75, 205, 1),
        child: ListView(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 70),
          children: <Widget>[
            buildHeader(
              urlImage: userGoogle?.photoUrl ??
                  'https://logowik.com/content/uploads/images/flutter5786.jpg',
              name: userGoogle?.displayName ?? '',
              email: userGoogle?.email ?? user?.email ?? '',
            ),
            Divider(
              color: Colors.white,
            ),
            SizedBox(
              height: 30,
            ),
            buildMenuItem(
              text: 'Home',
              icon: Icons.home,
              onClicked: () => selectedItem(context, 0),
            ),
            SizedBox(
              height: 16,
            ),
            buildMenuItem(
              text: 'About',
              icon: Icons.people,
              onClicked: () => selectedItem(context, 1),
            ),
            Divider(
              color: Colors.white,
            ),
            SizedBox(
              height: 16,
            ),
            buildMenuItem(
              text: 'Logout',
              icon: Icons.logout,
              onClicked: () => selectedItem(context, 2),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildHeader({
    String name,
    String email,
    String urlImage,
  }) =>
      InkWell(
        onTap: () {},
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: 30,
            ),
            CircleAvatar(
              radius: 30,
              backgroundImage: NetworkImage(urlImage),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              name,
              style: TextStyle(fontSize: 16, color: Colors.white),
            ),
            Text(
              email,
              style: TextStyle(fontSize: 14, color: Colors.white),
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      );

  Widget buildMenuItem({
    String text,
    IconData icon,
    VoidCallback onClicked,
  }) {
    final color = Colors.white;
    final hoverColor = Colors.white70;

    return ListTile(
      leading: Icon(icon, color: color),
      title: Text(
        text,
        style: TextStyle(color: color),
      ),
      hoverColor: hoverColor,
      onTap: onClicked,
    );
  }

  void selectedItem(BuildContext context, int index) {
    switch (index) {
      case 0:
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => (BtnNav())),
        );
        break;

      case 1:
        Navigator.pop(context);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => About()),
        );
        break;
      case 2:
        final GoogleSignInAccount userGoogle = googleSignIn.currentUser;

        _handleSignOut().whenComplete(() => Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => (Login())),
            ));

        AuthenticationHelper().signOut().then(
              (_) => Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (context) => Login()),
              ),
            );
    }
  }
}
