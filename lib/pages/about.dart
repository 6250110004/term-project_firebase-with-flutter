import 'package:flutter/material.dart';

class About extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("about"),
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              sbox(),
              stdGroup(
                assetImage: AssetImage('assets/images/karn.jpg'),
                stdID: '6250110002',
                stdName: 'นายณัฐกานต์ ขุนสามัญ',
              ),
              sbox(),
              stdGroup(
                assetImage: AssetImage('assets/images/por.jpg'),
                stdID: '6250110004',
                stdName: 'นายนรพิชญ์ พงษ์สุวรรณ',
              ),
              sbox(),
              stdGroup(
                assetImage: AssetImage('assets/images/m.jpg'),
                stdID: '6250110007',
                stdName: 'นายพัชรพล จันชนะพล',
              ),
              sbox(),
              stdGroup(
                assetImage: AssetImage('assets/images/rach.jpg'),
                stdID: '6250110010',
                stdName: 'นางสาวรัชนีกร สุกใส',
              ),
              sbox(),
              stdGroup(
                assetImage: AssetImage('assets/images/dif.jpg'),
                stdID: '6250110027',
                stdName: 'นายพชร นกต่อ',
              ),
              sbox(),
            ],
          ),
        ),
      ),
    );
  }
}

class sbox extends StatelessWidget {
  const sbox({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 24,
    );
  }
}

class stdGroup extends StatelessWidget {
  const stdGroup({
    this.assetImage,
    this.stdID,
    this.stdName,
    Key key,
  }) : super(key: key);

  final AssetImage assetImage;
  final String stdID;
  final String stdName;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 32),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        color: Color.fromARGB(255, 42, 95, 163),
      ),
      padding: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircleAvatar(
                radius: 30,
                backgroundImage: assetImage,
              ),
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Text(
            stdID,
            style: TextStyle(
                fontSize: 18,
                color: Colors.white,
                fontWeight: FontWeight.w700,
                letterSpacing: 1.0),
          ),
          SizedBox(
            height: 7,
          ),
          Text(
            stdName,
            style: TextStyle(
                fontSize: 18,
                color: Colors.white,
                fontWeight: FontWeight.w600,
                letterSpacing: 1.0),
          ),
        ],
      ),
    );
  }
}
